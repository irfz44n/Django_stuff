var playerBlue = prompt("PLayer Blue Enter your name");
var playerRed = prompt("PLayer Red Enter your name");

// var playerRed = "player_red";
// var playerBlue = "player_blue";

$('h3').text("It's " + playerBlue + "\'s Turn");

var Table = $('table tr'); //tr inside table

var _blue = "rgb(0, 0, 255)";
var _red = "rgb(255, 0, 0)";

var player= 1; //it's either player 1 or player -1;


function check_bottom(index){
  for(var i=5;i>=0;i--){
    if (Table.eq(i).find('td').eq(index).find('button').css('background-color') == "rgb(128, 128, 128)"){
      return i;
    }
  }
  return -1;
}

function addCoin(col,row,color){
  Table.eq(row).find('td').eq(col).find('button').css('background-color',color);
}

function check_color(col,row){
  return Table.eq(row).find('td').eq(col).find('button').css('background-color');
}

function check_horizontal_win(){
  for (var i=0;i<6;i++){
    for (var j=0;j<4;j++){
      if (check_color(j,i) == "rgb(255, 0, 0)" &&
          check_color(j+1,i) == "rgb(255, 0, 0)" &&
          check_color(j+2,i) == "rgb(255, 0, 0)" &&
          check_color(j+3,i) == "rgb(255, 0, 0)"){
          return playerRed;
      }else if(check_color(j,i) == "rgb(0, 0, 255)" &&
                check_color(j+1,i) == "rgb(0, 0, 255)" &&
                check_color(j+2,i) == "rgb(0, 0, 255)" &&
                check_color(j+3,i) == "rgb(0, 0, 255)"){
          return playerBlue;
      }
    }
  }
  return " ";
}

function check_vertical_win(){
  for (var i=0;i<3;i++){
    for (var j=0;j<7;j++){
      if (check_color(j,i) == "rgb(255, 0, 0)" &&
          check_color(j,i+1) == "rgb(255, 0, 0)" &&
          check_color(j,i+2) == "rgb(255, 0, 0)" &&
          check_color(j,i+3) == "rgb(255, 0, 0)"){
          return playerRed;
      }else if (check_color(j,i) == "rgb(0, 0, 255)" &&
          check_color(j,i+1) == "rgb(0, 0, 255)" &&
          check_color(j,i+2) == "rgb(0, 0, 255)" &&
          check_color(j,i+3) == "rgb(0, 0, 255)"){
          return playerBlue;
      }
    }
  }
  return " ";
}

function check_diagonal_wins(){
  for (var i=0;i<6;i++){
    for (var j=0;j<7;j++){
      if (check_color(j,i) == "rgb(255, 0, 0)" &&
          check_color(j+1,i+1) == "rgb(255, 0, 0)" &&
          check_color(j+2,i+2) == "rgb(255, 0, 0)" &&
          check_color(j+3,i+3) == "rgb(255, 0, 0)"){
          return playerRed;
      }else if (check_color(j,i) == "rgb(0, 0, 255)" &&
          check_color(j+1,i+1) == "rgb(0, 0, 255)" &&
          check_color(j+2,i+2) == "rgb(0, 0, 255)" &&
          check_color(j+3,i+3) == "rgb(0, 0, 255)"){
          return playerBlue;
      }else if(check_color(j,i) == "rgb(255, 0, 0)" &&
                check_color(j+1,i-1) == "rgb(255, 0, 0)" &&
                check_color(j+2,i-2) == "rgb(255, 0, 0)" &&
                check_color(j+3,i-3) == "rgb(255, 0, 0)"){
          return playerRed;
      }else if(check_color(j,i) == "rgb(0, 0, 255)" &&
                check_color(j+1,i-1) == "rgb(0, 0, 255)" &&
                check_color(j+2,i-2) == "rgb(0, 0, 255)" &&
                check_color(j+3,i-3) == "rgb(0, 0, 255)"){
          return playerBlue;
      }
    }
  }
  return " ";
}


//button click event
$('button').on('click',function(){
  var indx = $(this).closest('td').index();
  var bottom = check_bottom(indx);
  if (bottom !== -1){
    if (player == 1){
      $('h3').text("It's now " + playerRed + "\'s Turn");
      addCoin(indx,bottom,_blue);
    }else{
      $('h3').text("It's now " + playerBlue + "\'s Turn");
      addCoin(indx,bottom,_red);
    }
    player *= -1;
  }else{
    $('h3').text("This row is full,Try other rows");
  }


  if (check_horizontal_win() !== " "){
    $('h3').text(check_horizontal_win() + " Won!");
    $('table').fadeOut(2000);
  }else if(check_vertical_win() !== " "){
    $('h3').text(check_vertical_win() + " Won!");
    $('table').fadeOut(2000);
  }else if(check_diagonal_wins() !== " "){
    $('h3').text(check_diagonal_wins() + " Won! the game");
    $('table').fadeOut(2000);
  }
});
