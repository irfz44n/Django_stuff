#!/usr/bin/env python
from random import randint

#generate Random 3 digit number.
number = [str(randint(0,9)),str(randint(0,9)),str(randint(0,9))]


guess_number = ""
retry_counter = 0

def take_guess():
    return list(input("What is your guess? "))

#Game logic
print("Welcome to codebreaker game!")
print("A 3 digit number is generated please try to guess the number.")
print("Clues will be provided as you try.")
similar_flag = 0
exit_flag = 0
while exit_flag == 0 :
    retry_counter+=1
    guess_number = take_guess()
    if len(guess_number) != 3:
        print("Please enter a 3 digit number")
    else:
        i=0
        while(i<3):
            if number[i] == guess_number[i]:
                similar_flag += 1
            i +=1


    if similar_flag == 1:
        print("One Number at right position\nPlease retry")
        similar_flag = 0
    elif similar_flag == 2:
        print("Two Numbers at right position\nPlease retry")
        similar_flag = 0
    elif similar_flag == 3:
        print("Congrats! you guessed the right number.")
        print("It took you {} tries to correctly guess the number".format(retry_counter))
        similar_flag = 0
        exit_flag = 1
    else:
        print("No matching number found\nPlease retry")

print(retry_counter)
